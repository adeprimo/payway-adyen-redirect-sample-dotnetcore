# Payway Adyen Redirect Sample #

### What is this repository for? ###

* For testing Payway Adyen Redirect Provider integration.


### How do I get set up? ###

* Build solution
* If not exists, create appsettings.Development.json with content (fill out the missing fields):

```javascript
{
  "Logging": {
    "LogLevel": {
      "Default": "Information",
      "Microsoft": "Warning",
      "Microsoft.Hosting.Lifetime": "Information"
    }
  },
  "ClientId": "",
  "ClientSecret": "",
  "DestinationClientId": "",
  "Scopes": "",
  "Locale": "",
  "OrganisationId": "",
  "ProductCode": "",
  "PeriodId": "",
  "Description": "",
  "CompleteUrl": "",
  "ErrorUrl": "",
  "CancelUrl": "",
  "PaywayApiBaseAddress": "",
  "ApiBaseAddress": "",
  "ProviderOriginUrl": "",
  "PaywayAdyenProviderUri": ""
}
```


### Need help? ###

* Contact [Payway Support](https://docs.worldoftulo.com/payway/support/)