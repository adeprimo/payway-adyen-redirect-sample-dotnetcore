﻿using Newtonsoft.Json;

namespace PaywayAdyenRedirectSample.Models
{
    public class AddressModel
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("first_name")]
        public string FirstName { get; set; }
        [JsonProperty("last_name")]
        public string LastName { get; set; }
        [JsonProperty("street")]
        public string Street { get; set; }
        [JsonProperty("street_number")]
        public string StreetNumber { get; set; }
        [JsonProperty("staircase")]
        public string Staircase { get; set; }
        [JsonProperty("floor")]
        public string Floor { get; set; }
        [JsonProperty("apartment_number")]
        public string ApartmentNumber { get; set; }
        [JsonProperty("zip_code")]
        public string ZipCode { get; set; }
        [JsonProperty("city")]
        public string City { get; set; }
        [JsonProperty("country_code")]
        public string CountryCode { get; set; }
        [JsonProperty("drop_box")]
        public string DropBox { get; set; }

        public AddressModel() { }
    }
}
