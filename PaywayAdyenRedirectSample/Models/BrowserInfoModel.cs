﻿using Newtonsoft.Json;

namespace PaywayAdyenRedirectSample.Models
{
    public class BrowserInfoModel
    {
        [JsonProperty("browser_ip")]
        public string IpAddress { get; set; }
        [JsonProperty("browser_language")]
        public string Language { get; set; }
        [JsonProperty("browser_user_agent")]
        public string Useragent { get; set; }

        public BrowserInfoModel(string ip, string langugage, string userAgent)
        {
            IpAddress = ip;
            Language = langugage;
            Useragent = userAgent;
        }
    }
}