﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using PaywayAdyenRedirectSample.Lib;
using PaywayAdyenRedirectSample.Models;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace PaywayAdyenRedirectSample.Controllers
{
    public class HomeController : Controller
    {
        private readonly IPaywayApiRepository _paywayApiRepository;
        private readonly IIdentityApiClient _identityApiClient;
        private readonly IConfiguration _configuration;
        private readonly ISessionManager _sessionManager;

        public HomeController(IPaywayApiRepository paywayApiRepository, IIdentityApiClient identityApiClient, IConfiguration configuration, ISessionManager sessionManager)
        {
            _paywayApiRepository = paywayApiRepository;
            _identityApiClient = identityApiClient;
            _configuration = configuration;
            _sessionManager = sessionManager;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View(new IndexViewModel { LoggedIn = !string.IsNullOrEmpty(_sessionManager.GetAccessToken()) });
        }

        [HttpPost]
        public async Task<IActionResult> Index(IndexViewModel model)
        {
            var clientId = _configuration.GetValue<string>("ClientId");
            var clientSecret = _configuration.GetValue<string>("ClientSecret");
            var scope = _configuration.GetValue<string>("Scopes").Split(" ");
            var organisationId = _configuration.GetValue<string>("OrganisationId");
            var token = await _identityApiClient.AccessToken(clientId, clientSecret, scope, organisationId, model.Username, model.Password);
            _sessionManager.SetAccessToken(token.Token);
            _sessionManager.SetRefreshToken(token.RefreshToken);

            return View(new IndexViewModel { LoggedIn = !string.IsNullOrEmpty(_sessionManager.GetAccessToken()) });
        }

        [HttpPost]
        public async Task<IActionResult> Buy()
        {
            var accessToken = await _identityApiClient.RedeemRefreshToken(_sessionManager.GetRefreshToken(), _configuration.GetValue<string>("ClientId"), _configuration.GetValue<string>("ClientSecret"), _configuration.GetValue<string>("Scopes").Split(" "));
            _sessionManager.SetAccessToken(accessToken.Token);

            var ticket = await _identityApiClient.CreateTicket(_sessionManager.GetAccessToken(), _configuration.GetValue<string>("DestinationClientId"));
            var productCode = _configuration.GetValue<string>("ProductCode");
            var periodId = _configuration.GetValue<string>("PeriodId");
            var description = _configuration.GetValue<string>("Description");
            var completeUrl = new Uri(_configuration.GetValue<string>("CompleteUrl"));
            var errorUrl = new Uri(_configuration.GetValue<string>("ErrorUrl"));
            var cancelUrl = new Uri(_configuration.GetValue<string>("CancelUrl"));
            var origin = new Uri(_configuration.GetValue<string>("ProviderOriginUrl"));
            var deliveryAddress = new AddressModel
            {
                FirstName = "FirstName",
                LastName = "LastName",
                Street = "The street",
                StreetNumber = "2",
                ZipCode = "12345",
                City = "The city",
                CountryCode = "SE"

            };
            var purchase_response = await _paywayApiRepository.InitializePurchase(_sessionManager.GetAccessToken(), productCode, periodId, description, completeUrl, errorUrl, cancelUrl, origin, deliveryAddress);
            var redirect_uri = BuildRedirectUri(ticket.Ticket, purchase_response.Id, _configuration.GetValue<string>("OrganisationId"));

            return Redirect(redirect_uri.ToString());
        }

        [HttpPost]
        public async Task<IActionResult> Verify()
        {
            var accessToken = await _identityApiClient.RedeemRefreshToken(_sessionManager.GetRefreshToken(), _configuration.GetValue<string>("ClientId"), _configuration.GetValue<string>("ClientSecret"), _configuration.GetValue<string>("Scopes").Split(" "));
            _sessionManager.SetAccessToken(accessToken.Token);

            var ticket = await _identityApiClient.CreateTicket(_sessionManager.GetAccessToken(), _configuration.GetValue<string>("DestinationClientId"));
            var orderId = _configuration.GetValue<string>("OrderId");
            var completeUrl = new Uri(_configuration.GetValue<string>("CompleteUrl"));
            var errorUrl = new Uri(_configuration.GetValue<string>("ErrorUrl"));
            var cancelUrl = new Uri(_configuration.GetValue<string>("CancelUrl"));
            var origin = new Uri(_configuration.GetValue<string>("ProviderOriginUrl"));
            var ipAddress = Request.HttpContext.Connection.RemoteIpAddress.ToString();
            var languages = Request.GetTypedHeaders().AcceptLanguage?.OrderByDescending(x => x.Quality ?? 1).Select(x => x.Value.ToString()).ToArray() ?? Array.Empty<string>();
            var userAgent = Request.Headers["User-Agent"].ToString();
            var browserInfo = new BrowserInfoModel(ipAddress == "::1" ? "127.0.0.1" : ipAddress, languages.First(), userAgent);

            var purchase_response = await _paywayApiRepository.InitializeVerification(_sessionManager.GetAccessToken(), orderId, completeUrl, errorUrl, cancelUrl, origin, browserInfo);
            var redirect_uri = BuildRedirectUri(ticket.Ticket, purchase_response.Id, _configuration.GetValue<string>("OrganisationId"));

            return Redirect(redirect_uri.ToString());
        }

        [HttpGet]
        public async Task<IActionResult> Receipt(string receiptId)
        {
            var accessToken = await _identityApiClient.RedeemRefreshToken(_sessionManager.GetRefreshToken(), _configuration.GetValue<string>("ClientId"), _configuration.GetValue<string>("ClientSecret"), _configuration.GetValue<string>("Scopes").Split(" "));
            _sessionManager.SetAccessToken(accessToken.Token);

            var receipt = await _paywayApiRepository.GetReceipt(accessToken.Token, receiptId);

            return View(receipt);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        private Uri BuildRedirectUri(string authorizationTicket, string paymentRef, string organisationId)
        {
            var digest = $"{authorizationTicket}:{paymentRef}:{organisationId}";

            byte[] bytes = System.Text.Encoding.UTF8.GetBytes(digest);
            string encoded = Convert.ToBase64String(bytes);

            return new Uri($"{_configuration.GetValue<string>("PaywayAdyenProviderUri")}/?d={encoded}");
        }
    }
}
