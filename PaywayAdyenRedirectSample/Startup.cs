using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using PaywayAdyenRedirectSample.Lib;
using System;
using System.Net.Http;
using System.Net.Http.Headers;

namespace PaywayAdyenRedirectSample
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var userAgent = "payway-adyen-provider-apiclient";

            services.AddDistributedMemoryCache();
            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromMinutes(30);
                options.Cookie.HttpOnly = true;
                options.Cookie.IsEssential = true;
                options.Cookie.Name = "PaywayAdyenRedirectSample";
                options.Cookie.SecurePolicy = CookieSecurePolicy.Always;
                options.Cookie.SameSite = SameSiteMode.None;
            });

            services.AddControllersWithViews();
            services.AddHttpContextAccessor();

            services.AddHttpClient("PaywayApiClient", client =>
            {
                var baseUrl = Configuration.GetValue<string>("PaywayApiBaseAddress");
                client.BaseAddress = new Uri(baseUrl);
                client.DefaultRequestHeaders.UserAgent.ParseAdd(userAgent);
                client.DefaultRequestHeaders.AcceptCharset.Add(new StringWithQualityHeaderValue("utf-8"));
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            });

            services.AddHttpClient("IdentityClient", client =>
            {
                var baseUrl = Configuration.GetValue<string>("ApiBaseAddress");
                client.BaseAddress = new Uri(baseUrl);
                client.DefaultRequestHeaders.UserAgent.ParseAdd(userAgent);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));
            });

            var sp = services.BuildServiceProvider();
            var httpClientFactory = (IHttpClientFactory)sp.GetService(typeof(IHttpClientFactory));

            services.AddScoped<IPaywayApiClient>(x =>
            {
                return new PaywayApiClient(httpClientFactory.CreateClient("PaywayApiClient"));
            });

            services.AddScoped<IIdentityApiClient>(x =>
            {
                return new IdentityApiClient(httpClientFactory.CreateClient("IdentityClient"));
            });

            services.AddSingleton(Configuration);
            services.AddScoped<ISessionManager, SessionManager>();
            services.AddScoped<IPaywayApiRepository, PaywayApiRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseSession();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                //app.UseExceptionHandler("/Home/Error");
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
                endpoints.MapControllerRoute(
                    name: "Receipt",
                    pattern: "/receipt",
                    defaults: new { controller = "Home", action = "Receipt" });
                endpoints.MapControllerRoute(
                    name: "Cancel",
                    pattern: "/cancel",
                    defaults: new { controller = "Home", action = "Cancel" });
                endpoints.MapControllerRoute(
                    name: "Error",
                    pattern: "/error",
                    defaults: new { controller = "Home", action = "Error" });
            });
        }
    }
}
