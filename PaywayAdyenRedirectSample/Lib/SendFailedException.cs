﻿using System;
using System.Net;

namespace PaywayAdyenRedirectSample.Lib
{
    public class SendFailedException : Exception
    {
        public HttpStatusCode StatusCode { get; private set; }
        public string Reason { get; private set; }

        public SendFailedException(HttpStatusCode statusCode, string reason, string message = null)
            : base($"HTTP {(int)statusCode} : {reason} - {message}")
        {
            StatusCode = statusCode;
            Reason = reason;
        }
    }
}
