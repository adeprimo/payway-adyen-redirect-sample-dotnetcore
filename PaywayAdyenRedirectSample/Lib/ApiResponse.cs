﻿using Newtonsoft.Json.Linq;
using System.Net;

namespace PaywayAdyenRedirectSample.Lib
{
    public class ApiResponse
    {
        public HttpStatusCode StatusCode { get; private set; }
        public JObject ResponseBody { get; private set; }

        public ApiResponse(HttpStatusCode statusCode, string responseBody)
        {
            StatusCode = statusCode;
            ResponseBody = JObject.Parse(responseBody);
        }

        public JToken Item
        {
            get { return ResponseBody["item"]; }
        }

        public JArray Items
        {
            get { return ResponseBody.Value<JArray>("items"); }
        }

        public override string ToString()
        {
            return ResponseBody.ToString();
        }
    }
}
