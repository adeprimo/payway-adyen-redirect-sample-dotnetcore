﻿using Newtonsoft.Json;
using System;

namespace PaywayAdyenRedirectSample.Lib
{
    public class AccessTokenModel
    {
        [JsonProperty("access_token")]
        public string Token { get; set; }
        [JsonProperty("expires_in")]
        public double LifeTime { get; set; }
        [JsonProperty("scope")]
        public string Scope { get; set; }
        [JsonProperty("received")]
        public DateTime Received { get; set; }
        [JsonProperty("expired")]
        public bool Expired
        {
            get
            {
                if (LifeTime == 0)
                    return false;
                return Received.AddSeconds(LifeTime) < DateTime.UtcNow;
            }
        }

        public AccessTokenModel()
        {
            Received = DateTime.UtcNow;
        }
    }

    public class RefreshTokenModel : AccessTokenModel
    {
        [JsonProperty("refresh_token")]
        public string RefreshToken { get; set; }
    }
}
