﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace PaywayAdyenRedirectSample.Lib
{
    public interface ISessionManager
    {
        T Get<T>(string key);
        void Set(string key, object value);
        void Remove(string key);

        string GetAccessToken();
        void SetAccessToken(string value);

        string GetRefreshToken();
        void SetRefreshToken(string value);
    }

    public class SessionManager : ISessionManager
    {
        private readonly IHttpContextAccessor _accessor;

        private static readonly string ACCESS_TOKEN_KEY = "AccessToken";
        private static readonly string REFRESH_TOKEN_KEY = "RefreshToken";

        public SessionManager(IHttpContextAccessor accessor)
        {
            _accessor = accessor;
        }

        public T Get<T>(string key)
        {
            var value = _accessor.HttpContext.Session.GetString(key);
            if (string.IsNullOrEmpty(value))
                return default;

            return JsonConvert.DeserializeObject<T>(value);
        }

        public void Set(string key, object value)
        {
            if (value == null)
                return;

            var json = JsonConvert.SerializeObject(value);
            _accessor.HttpContext.Session.SetString(key, json);
        }

        public void Remove(string key)
        {
            _accessor.HttpContext.Session.Remove(key);
        }

        public string GetAccessToken()
        {
            return Get<string>(ACCESS_TOKEN_KEY);
        }

        public void SetAccessToken(string value)
        {
            Set(ACCESS_TOKEN_KEY, value);
        }

        public string GetRefreshToken()
        {
            return Get<string>(REFRESH_TOKEN_KEY);
        }

        public void SetRefreshToken(string value)
        {
            Set(REFRESH_TOKEN_KEY, value);
        }
    }
}
