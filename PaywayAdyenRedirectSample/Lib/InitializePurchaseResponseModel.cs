﻿using Newtonsoft.Json;
using PaywayAdyenRedirectSample.Models;
using System;

namespace PaywayAdyenRedirectSample.Lib
{
    public class InitializePurchaseResponseModel
    {
        [JsonProperty("id")]
        public string Id { get; set; }
    }

    public class InitializePurchaseRequestModel
    {
        [JsonProperty("period_id")]
        public string PeriodId { get; set; }
        [JsonProperty("product_code")]
        public string ProductCode { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
        [JsonProperty("complete_url")]
        public string CompleteUrl { get; set; }
        [JsonProperty("cancel_url")]
        public string CancelUrl { get; set; }
        [JsonProperty("error_url")]
        public string ErrorUrl { get; set; }
        [JsonProperty("origin")]
        public string Origin { get; set; }
        [JsonProperty("delivery_address")]
        public AddressModel DeliveryAddress { get; set; }
        [JsonProperty("traffic_source")]
        public string TrafficSource { get; set; }
        [JsonProperty("discount_code")]
        public string DiscountCode { get; set; }
        [JsonProperty("subscription_start_date")]
        public DateTime SubscriptionStartDate { get; set; }
    }
}
