﻿using Newtonsoft.Json;

namespace PaywayAdyenRedirectSample.Lib
{
    public class TicketModel
    {
        [JsonProperty("ticket")]
        public string Ticket { get; set; }
    }
}
