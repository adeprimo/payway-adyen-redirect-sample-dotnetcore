﻿using Newtonsoft.Json;

namespace PaywayAdyenRedirectSample.Lib
{
    public class ReceiptModel
    {
        [JsonProperty("order_id")]
        public string OrderId { get; set; }
    }
}
