﻿using Newtonsoft.Json;
using PaywayAdyenRedirectSample.Models;

namespace PaywayAdyenRedirectSample.Lib
{
    public class InitializeVerificationResponseModel
    {
        [JsonProperty("id")]
        public string Id { get; set; }
    }

    public class InitializeVerificationRequestModel
    {
        [JsonProperty("order_id")]
        public string OrderId { get; set; }
        [JsonProperty("complete_url")]
        public string CompleteUrl { get; set; }
        [JsonProperty("cancel_url")]
        public string CancelUrl { get; set; }
        [JsonProperty("error_url")]
        public string ErrorUrl { get; set; }
        [JsonProperty("origin")]
        public string Origin { get; set; }
        [JsonProperty("browser_info")]
        public BrowserInfoModel BrowserInfo { get; set; }
    }    
}
