﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace PaywayAdyenRedirectSample.Lib
{
    public interface IIdentityApiClient
    {
        Task<RefreshTokenModel> AccessToken(string clientId, string clientSecret, IEnumerable<string> scopes, string organisation_id, string username, string password);
        Task<RefreshTokenModel> RedeemRefreshToken(string refreshToken, string clientId, string clientSecret, IEnumerable<string> scopes);
        Task<TicketModel> CreateTicket(string accessToken, string destinationClientId);
    }

    public class IdentityApiClient : IIdentityApiClient
    {
        public readonly HttpClient _httpClient;

        public IdentityApiClient(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<RefreshTokenModel> AccessToken(string clientId, string clientSecret, IEnumerable<string> scopes, string organisation_id, string username, string password)
        {
            var uri = $"{_httpClient.BaseAddress}/authorization/access_token";
            var scope = HttpUtility.UrlPathEncode(HttpUtility.UrlEncode(string.Join(" ", scopes)));
            var payload = $"client_id={clientId}&client_secret={clientSecret}&grant_type=password&scope={scope}&username={organisation_id}^{username}&password={password}";

            var request = new HttpRequestMessage(HttpMethod.Post, uri);
            request.Content = new ByteArrayContent(Encoding.ASCII.GetBytes(payload));

            var response = await _httpClient.SendAsync(request);
            var body = await response.Content.ReadAsStringAsync();

            if (!response.IsSuccessStatusCode)
                throw new SendFailedException(response.StatusCode, response.ReasonPhrase, body);

            return JsonConvert.DeserializeObject<RefreshTokenModel>(body);
        }

        public async Task<TicketModel> CreateTicket(string accessToken, string destinationClientId)
        {
            var uri = $"{_httpClient.BaseAddress}/authorization/ticket";
            var payload = $"client_id={destinationClientId}";
            var request = new HttpRequestMessage(HttpMethod.Post, uri);
            request.Headers.Authorization = new AuthenticationHeaderValue("OAuth", accessToken);
            request.Content = new ByteArrayContent(Encoding.ASCII.GetBytes(payload));

            var response = await _httpClient.SendAsync(request);
            var body = await response.Content.ReadAsStringAsync();

            if (!response.IsSuccessStatusCode)
                throw new SendFailedException(response.StatusCode, response.ReasonPhrase, body);

            return JsonConvert.DeserializeObject<TicketModel>(body);
        }

        public async Task<RefreshTokenModel> RedeemRefreshToken(string refreshToken, string clientId, string clientSecret, IEnumerable<string> scopes)
        {
            var uri = $"{_httpClient.BaseAddress}/authorization/access_token";
            var scope = HttpUtility.UrlPathEncode(HttpUtility.UrlEncode(string.Join(" ", scopes)));
            var payload = $"refresh_token={refreshToken}&client_id={clientId}&client_secret={clientSecret}&grant_type=refresh_token&scope={scope}";

            var request = new HttpRequestMessage(HttpMethod.Post, uri);
            request.Content = new ByteArrayContent(Encoding.ASCII.GetBytes(payload));

            var response = await _httpClient.SendAsync(request);
            var body = await response.Content.ReadAsStringAsync();

            if (!response.IsSuccessStatusCode)
                throw new SendFailedException(response.StatusCode, response.ReasonPhrase, body);

            return JsonConvert.DeserializeObject<RefreshTokenModel>(body);
        }
    }
}
