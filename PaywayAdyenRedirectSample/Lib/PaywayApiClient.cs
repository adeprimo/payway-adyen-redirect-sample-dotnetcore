﻿using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace PaywayAdyenRedirectSample.Lib
{
    public interface IPaywayApiClient
    {
        Task<ApiResponse> Get(string accessToken, string path, Dictionary<string, string> headers = null);
        Task<ApiResponse> Post(string accessToken, string path, string json, Dictionary<string, string> headers = null);
    }

    public class PaywayApiClient : IPaywayApiClient
    {
        private readonly HttpClient _httpClient;

        public PaywayApiClient(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<ApiResponse> Get(string accessToken, string path, Dictionary<string, string> headers = null)
        {
            var uri = $"{_httpClient.BaseAddress}{path}";
            var request = new HttpRequestMessage(HttpMethod.Get, uri);
            request.Headers.Authorization = new AuthenticationHeaderValue("OAuth", accessToken);

            if (headers != null)
            {
                foreach (KeyValuePair<string, string> entry in headers)
                    request.Headers.Add(entry.Key, entry.Value);
            }

            var response = await _httpClient.SendAsync(request);
            var body = await response.Content.ReadAsStringAsync();

            if (!response.IsSuccessStatusCode)
                throw new SendFailedException(response.StatusCode, response.ReasonPhrase, body);

            return new ApiResponse(response.StatusCode, body);
        }

        public async Task<ApiResponse> Post(string accessToken, string path, string json, Dictionary<string, string> headers = null)
        {
            var uri = $"{_httpClient.BaseAddress}{path}";
            var request = new HttpRequestMessage(HttpMethod.Post, uri);
            request.Headers.Authorization = new AuthenticationHeaderValue("OAuth", accessToken);

            if (headers != null)
            {
                foreach (KeyValuePair<string, string> entry in headers)
                    request.Headers.Add(entry.Key, entry.Value);
            }

            request.Content = new ByteArrayContent(Encoding.UTF8.GetBytes(json));
            request.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            var response = await _httpClient.SendAsync(request);
            var body = await response.Content.ReadAsStringAsync();

            if (!response.IsSuccessStatusCode)
                throw new SendFailedException(response.StatusCode, response.ReasonPhrase, body);

            return new ApiResponse(response.StatusCode, body);
        }
    }
}
