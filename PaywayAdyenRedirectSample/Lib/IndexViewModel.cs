﻿namespace PaywayAdyenRedirectSample.Lib
{
    public class IndexViewModel
    {
        public string Username { get; set; }
        public string Password { get; set; }

        public bool LoggedIn { get; set; }
    }
}
