﻿using Newtonsoft.Json;
using PaywayAdyenRedirectSample.Models;
using System;
using System.Threading.Tasks;

namespace PaywayAdyenRedirectSample.Lib
{
    public interface IPaywayApiRepository
    {
        Task<InitializePurchaseResponseModel> InitializePurchase(string accessToken, string productCode, string periodId, string description, Uri completeUrl, Uri errorUrl, Uri cancelUrl, Uri origin, AddressModel deliveryAddress);
        Task<InitializeVerificationResponseModel> InitializeVerification(string accessToken, string orderId, Uri completeUrl, Uri errorUrl, Uri cancelUrl, Uri origin, BrowserInfoModel browserInfo);
        Task<ReceiptModel> GetReceipt(string accessToken, string receiptId);
    }

    public class PaywayApiRepository : IPaywayApiRepository
    {
        private readonly IPaywayApiClient _paywayApiClient;

        public PaywayApiRepository(IPaywayApiClient paywayApiClient)
        {
            _paywayApiClient = paywayApiClient;
        }

        public async Task<ReceiptModel> GetReceipt(string accessToken, string receiptId)
        {
            var path = $"/me/get_receipt?receipt_id={receiptId}";
            var response = await _paywayApiClient.Get(accessToken, path);

            return JsonConvert.DeserializeObject<ReceiptModel>(response.Item.ToString());
        }

        public async Task<InitializePurchaseResponseModel> InitializePurchase(string accessToken, string productCode, string periodId, string description, Uri completeUrl, Uri errorUrl, Uri cancelUrl, Uri origin, AddressModel deliveryAddress = null)
        {
            var path = $"/adyen/initialize_purchase";
            var payload = new InitializePurchaseRequestModel
            {
                PeriodId = periodId,
                ProductCode = productCode,
                Description = description,
                CompleteUrl = completeUrl.ToString(),
                ErrorUrl = errorUrl.ToString(),
                CancelUrl = cancelUrl.ToString(),
                Origin = origin.ToString(),
                DeliveryAddress = deliveryAddress,                
                SubscriptionStartDate = DateTime.Now
            };
            var response = await _paywayApiClient.Post(accessToken, path, JsonConvert.SerializeObject(payload));

            return JsonConvert.DeserializeObject<InitializePurchaseResponseModel>(response.ToString());
        }

        public async Task<InitializeVerificationResponseModel> InitializeVerification(string accessToken, string orderId, Uri completeUrl, Uri errorUrl, Uri cancelUrl, Uri origin, BrowserInfoModel browserInfo)
        {
            var path = $"/adyen/initialize_verification";
            var payload = new InitializeVerificationRequestModel
            {
                OrderId = orderId,
                CompleteUrl = completeUrl.ToString(),
                CancelUrl = cancelUrl.ToString(),
                ErrorUrl = errorUrl.ToString(),
                Origin = origin.ToString(),
                BrowserInfo = browserInfo
            };

            var response = await _paywayApiClient.Post(accessToken, path, JsonConvert.SerializeObject(payload));

            return JsonConvert.DeserializeObject<InitializeVerificationResponseModel>(response.ToString());
        }
    }
}
